# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):

    if age >= 18 and has_consent_form == True:
        return"you can skydive"

    else:
        return "you cant skydive"


maybe = can_skydive(18,True)

print(maybe)











# def max_of_three(value1, value2, value3):
#     if value1 > value2:
#         return value1

#     elif value2 > value3:
#         return value2

#     else:
#         return value3
# example_values = max_of_three(5,10,100)

# print(example_values)
